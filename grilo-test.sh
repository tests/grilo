#!/bin/sh

set -e
set -x

# This expects to run as root.
if [ "$(id -u)" -ne 0 ]; then
    echo "grilo must run as root"
    exit 2
fi

# Add the correct paths to test executables and libraries
. common/update-test-path

for dir in Documents Music Pictures Videos ; do
      if [ ! -d "${HOME}/${dir}" ]; then
            mkdir -p "${HOME}/${dir}"
      fi
done

RESOURCE_DIR="$(pwd)/TestResource"

#test resources if needed

cp -R "${RESOURCE_DIR}/images/" "${HOME}/Pictures/."
cp -R "${RESOURCE_DIR}/documents/" "${HOME}/Documents/."
cp -R "${RESOURCE_DIR}/audio/" "${HOME}/Music/."
cp -R "${RESOURCE_DIR}/playlists/" "${HOME}/Music/."
cp -R "${RESOURCE_DIR}/videos/" "${HOME}/Videos/."

browse=$(grl-launch-0.3 -k title browse grl-filesystem)

search=$(grl-launch-0.3 -k title search "generic-no-artwork" grl-filesystem)


if [ -f "${HOME}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg" ] ; then
       rm "${HOME}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
fi

if [ -f "a.txt" ] ; then
	rm "a.txt"
fi
touch a.txt

$(grl-launch-0.3 -k uri,url -T -S monitor grl-filesystem > a.txt &)
sleep 5

#file addition
cp "${RESOURCE_DIR}/images/320px-European_Common_Frog_Rana_temporaria.jpg" "${HOME}/Pictures/"
sleep 5

#file changed
echo "modify" > "${HOME}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
sleep 5

#file removed
rm "${HOME}/Pictures/320px-European_Common_Frog_Rana_temporaria.jpg"
sleep 5

#Kill background process
process_id=$(pgrep "grl-launch")
sudo kill -9 ${process_id}

echo "||TEST RESULTS||"

testname="grilotest-browse"
if [ "${browse}" = "No results" ]
then
      echo "${testname}: fail"
      exit 1
else
      echo "${testname}: pass"
fi

testname="grilotest-search"
if [ "${search}" = "No results" ]

then
      echo "${testname}: fail"
      exit 1
else
      echo "${testname}: pass"
fi

testname="grilotest-add_notification"
if [ $(cat a.txt | grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep added) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi

testname="grilotest-change_notification"
if [ $(cat a.txt | grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep changed | head -1) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi

testname="grilotest-remove_notification"
if [ $(cat a.txt | grep 320px-European_Common_Frog_Rana_temporaria.jpg | grep removed) ]
then
      echo "${testname}: pass"
else
      echo "${testname}: fail"
      exit 1
fi

#clean up
rm -R "${HOME}/Pictures/images"
rm -R "${HOME}/Documents/documents"
rm -R "${HOME}/Music/audio"
rm -R "${HOME}/Music/playlists"
rm -R "${HOME}/Videos/videos"
rm a.txt

